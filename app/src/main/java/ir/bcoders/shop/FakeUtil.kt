package ir.bcoders.shop

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.core.content.edit
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import ir.bcoders.shop.models.*
import java.io.BufferedWriter
import java.io.File
import java.io.OutputStreamWriter
import java.util.*
import java.util.regex.Pattern
import kotlin.random.Random

private const val TAG = "FakeUtil"

private val imageUrlList: MutableList<String> = mutableListOf()

var iUrl = "https://www.digikala.com/"

fun randomImageUrl(): String {
    if (imageUrlList.isEmpty()) {
        val rx = "(http(s?):)([/|.|\\w|\\s|-])*\\.(?:jpg|gif|png)"
        val url = ConvertUrlToString.convert(iUrl)
        val pat = Pattern.compile(rx)
        val matcher = pat.matcher(url)
        while (matcher.find()) {
            imageUrlList.add(matcher.group())
        }
    }

    return imageUrlList[Random.nextInt(imageUrlList.size)]
}

private val userIcons = listOf(
    "https://images.unsplash.com/photo-1506744038136-46273834b3fb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
    "https://www.glenstone.org/wp-content/uploads/prod/2018/07/AV_Landscape-Hero-Contour-2993-1276x800.jpg",
    "https://images.unsplash.com/photo-1532274402911-5a369e4c4bb5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
    "https://i0.wp.com/digital-photography-school.com/wp-content/uploads/2019/02/Landscapes-04-jeremy-flint.jpg?fit=1500%2C1000&ssl=1",
    "https://static.photocdn.pt/images/articles/2017_1/iStock-545347988.jpg",
    "https://prod-discovery.edx-cdn.org/media/course/image/93f11b63-0c29-4472-964e-c6db1cc574e8-7ad96686a6fd.small.jpg",
    "http://algonquin1.com/wp-content/uploads/2016/03/wallpaper-fantasy-summer-landscape-background-computer-213806.jpg",
    "https://static.photocdn.pt/images/articles/2018/03/09/articles/2017_8/landscape_photography.jpg",
    "https://static.seattletimes.com/wp-content/uploads/2018/10/90a2c67c-ba17-11e8-b2d9-c270ab1caed2-1020x776.jpg"
)

fun randomUserImageUrl(): String {
    return userIcons[Random.nextInt(userIcons.size)]
}

private val chars = arrayOf(
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
    " ",
    " ",
    " ",
    " "
)

fun randomString(size: Int): String {
    var out = ""
    for (i in 0 until size) {
        out += chars[Random.nextInt(if (i > 0) chars.size else chars.size - 4)]
        if (i == 0) out = out.toUpperCase(Locale.getDefault())
    }

    return out
}

fun fakeCategories(id: Int) = HomeItem.Categories(id)

fun fakeProducts(id: Int) = HomeItem.Products(
    id,
    listOf("Top Selling Today", "For You", "New in Shop", "Shopping for School")[id % 4]
)

fun fakeOffer(id: Int) = HomeItem.Offer(
    id,
    randomString(10),
    randomString(25),
    randomImageUrl(),
    Random.nextInt(0xFFFFFF)
)

fun fakeCategory(id: Int) = Category(id, randomString(10), randomImageUrl())

fun fakeSubCategory(id: Int) = SubCategory(
    id,
    randomString(10),
    randomString(50),
    randomImageUrl(),
    Random.nextInt()
)

fun fakeProduct(id: Int): Product = Product(
    id,
    randomString(10),
    randomImageUrl(),
    Random.nextDouble(3.0, 5.0).toFloat(),
    Random.nextInt(1000000),
    Random.nextDouble(5.00, 100.00),
    randomString(1000),
    Random.nextInt(100000),
    Random.nextInt(100000),
    Random.nextInt(100000),
    Random.nextInt(100000),
    Random.nextInt(100000)
)

fun fakeReview(id: Int) = Review(
    id,
    randomUserImageUrl(),
    randomString(10),
    Random.nextDouble(2.0, 5.0).toFloat(),
    Random.nextInt(34568),
    System.currentTimeMillis(),
    randomString(100)
)

private fun writeFile(context: Context, name: String, data: String) {
    BufferedWriter(
        OutputStreamWriter(
            context.openFileOutput(
                name,
                Context.MODE_PRIVATE
            )
        )
    ).apply {
        write(data)
        close()
    }
}
