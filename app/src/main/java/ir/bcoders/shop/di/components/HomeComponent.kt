package ir.bcoders.shop.di.components

import dagger.Component
import ir.bcoders.shop.di.HomeScope
import ir.bcoders.shop.di.modules.HomeModule
import ir.bcoders.shop.di.modules.ViewModelsModule
import ir.bcoders.shop.fragments.HomeFragment

@HomeScope
@Component(
    modules = [HomeModule::class, ViewModelsModule::class],
    dependencies = [ApplicationComponent::class]
)
interface HomeComponent {

    fun inject(fragment: HomeFragment)
}
