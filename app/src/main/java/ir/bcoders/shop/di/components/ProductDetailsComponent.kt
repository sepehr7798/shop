package ir.bcoders.shop.di.components

import dagger.BindsInstance
import dagger.Component
import ir.bcoders.shop.di.ProductDetailsScope
import ir.bcoders.shop.di.modules.ProductDetailsModule
import ir.bcoders.shop.di.modules.ViewModelsModule
import ir.bcoders.shop.fragments.ProductDetailsFragment
import ir.bcoders.shop.models.Product

@ProductDetailsScope
@Component(
    modules = [ProductDetailsModule::class, ViewModelsModule::class],
    dependencies = [ApplicationComponent::class]
)
interface ProductDetailsComponent {

    fun inject(fragment: ProductDetailsFragment)

    @Component.Builder
    interface Builder {

        fun applicationComponent(applicationComponent: ApplicationComponent): Builder

        fun build(): ProductDetailsComponent
    }
}