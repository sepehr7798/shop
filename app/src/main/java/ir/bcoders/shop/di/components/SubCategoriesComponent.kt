package ir.bcoders.shop.di.components

import dagger.BindsInstance
import dagger.Component
import ir.bcoders.shop.di.SubCategoriesScope
import ir.bcoders.shop.di.modules.SubCategoriesModule
import ir.bcoders.shop.di.modules.ViewModelsModule
import ir.bcoders.shop.fragments.SubCategoriesFragment
import ir.bcoders.shop.models.Category

@SubCategoriesScope
@Component(
    modules = [SubCategoriesModule::class, ViewModelsModule::class],
    dependencies = [ApplicationComponent::class]
)
interface SubCategoriesComponent {

    fun inject(fragment: SubCategoriesFragment)

    @Component.Builder
    interface Builder {

        fun applicationComponent(applicationComponent: ApplicationComponent): Builder

        fun build(): SubCategoriesComponent
    }
}