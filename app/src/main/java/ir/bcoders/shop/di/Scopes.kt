package ir.bcoders.shop.di

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class HomeScope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class SubCategoriesScope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ProductsScope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ProductDetailsScope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ReviewsScope