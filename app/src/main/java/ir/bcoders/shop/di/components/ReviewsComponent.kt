package ir.bcoders.shop.di.components

import androidx.fragment.app.Fragment
import dagger.BindsInstance
import dagger.Component
import ir.bcoders.shop.di.ReviewsScope
import ir.bcoders.shop.di.modules.ReviewsModule
import ir.bcoders.shop.di.modules.ViewModelsModule
import ir.bcoders.shop.fragments.ReviewsFragment
import ir.bcoders.shop.models.Product

@ReviewsScope
@Component(
    modules = [ReviewsModule::class, ViewModelsModule::class],
    dependencies = [ApplicationComponent::class]
)
interface ReviewsComponent {

    fun inject(fragment: ReviewsFragment)

    @Component.Builder
    interface Builder {

        fun applicationComponet(applicationComponent: ApplicationComponent): Builder

        fun build(): ReviewsComponent
    }
}