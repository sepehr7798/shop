package ir.bcoders.shop.di.modules

import android.content.Context
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import dagger.Module
import dagger.Provides
import ir.bcoders.shop.di.ApplicationContext
import ir.bcoders.shop.repository.Repository
import ir.bcoders.shop.repository.RepositoryImpl
import ir.bcoders.shop.repository.network.ShopService
import javax.inject.Singleton

@Module
object ApplicationModule {

    @JvmStatic
    @Provides
    @Singleton
    fun provideOkHttp3Downloader(@ApplicationContext context: Context): OkHttp3Downloader =
        OkHttp3Downloader(context, Long.MAX_VALUE)

    @JvmStatic
    @Provides
    @Singleton
    fun providePicasso(
        @ApplicationContext context: Context,
        okHttp3Downloader: OkHttp3Downloader
    ): Picasso = Picasso.Builder(context)
        .downloader(okHttp3Downloader)
        .build().apply {
            setIndicatorsEnabled(true)
            isLoggingEnabled = true
        }

    @JvmStatic
    @Singleton
    @Provides
    fun provideRepsitory(shopService: ShopService): Repository = RepositoryImpl(shopService)
}