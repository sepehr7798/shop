package ir.bcoders.shop.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import ir.bcoders.shop.ProjectViewModelFactory
import ir.bcoders.shop.di.*
import ir.bcoders.shop.viewmodels.*
import ir.bcoders.shop.viewmodels.impls.*
import ir.bcoders.shop.viewmodels.testimpls.*
import javax.inject.Provider
import javax.inject.Singleton

@Module
abstract class ViewModelsModule {

    @Binds
    abstract fun provideViewModelFactory(factory: ProjectViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun provideHomeViewModel(viewModel: HomeViewModelImpl): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SubCategoriesViewModel::class)
    abstract fun provideSubCategoriesViewModel(viewModel: SubCategoriesViewModelImpl): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductsViewModel::class)
    abstract fun provideProductsViewModel(viewModel: ProductsViewModelImpl): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProductDetailsViewModel::class)
    abstract fun provideProductDetailsViewModel(viewModel: ProductDetailsViewModelImpl): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ReviewsViewModel::class)
    abstract fun provideReviewsViewModel(viewModel: ReviewsViewModelImpl): ViewModel
}