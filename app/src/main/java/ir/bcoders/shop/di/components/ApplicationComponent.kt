package ir.bcoders.shop.di.components

import android.content.Context
import androidx.lifecycle.ViewModelProvider
import dagger.BindsInstance
import dagger.Component
import ir.bcoders.shop.MainApplication
import ir.bcoders.shop.di.ApplicationContext
import ir.bcoders.shop.di.modules.ApplicationModule
import ir.bcoders.shop.di.modules.NetworkModule
import ir.bcoders.shop.di.modules.ViewModelsModule
import ir.bcoders.shop.repository.Repository
import javax.inject.Singleton

@Singleton
@Component(
    modules = [ApplicationModule::class, NetworkModule::class]
)
interface ApplicationComponent {

    fun inject(application: MainApplication)

    fun repository(): Repository

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun applicationContext(@ApplicationContext applicationContext: Context): Builder

        fun build(): ApplicationComponent
    }
}