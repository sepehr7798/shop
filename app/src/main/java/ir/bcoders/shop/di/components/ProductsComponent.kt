package ir.bcoders.shop.di.components

import dagger.BindsInstance
import dagger.Component
import ir.bcoders.shop.di.ProductsScope
import ir.bcoders.shop.di.modules.ProductsModule
import ir.bcoders.shop.di.modules.ViewModelsModule
import ir.bcoders.shop.fragments.ProductsFragment
import ir.bcoders.shop.models.SubCategory

@ProductsScope
@Component(
    modules = [ProductsModule::class, ViewModelsModule::class],
    dependencies = [ApplicationComponent::class]
)
interface ProductsComponent {

    fun inject(fragment: ProductsFragment)

    @Component.Builder
    interface Builder {

        fun applicationComponent(applicationComponent: ApplicationComponent): Builder

        fun build(): ProductsComponent
    }
}