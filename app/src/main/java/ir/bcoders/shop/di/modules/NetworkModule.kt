package ir.bcoders.shop.di.modules

import dagger.Module
import dagger.Provides
import ir.bcoders.shop.repository.network.ShopService
import ir.bcoders.shop.repository.network.StatableLiveListCallAdapterFactory
import ir.bcoders.shop.repository.network.TestInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
object NetworkModule {

    @JvmStatic
    @Provides
    @Singleton
    fun providesOkHttpClient(testInterceptor: TestInterceptor): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(testInterceptor)
            .connectTimeout(10000, TimeUnit.MILLISECONDS)
            .writeTimeout(10000, TimeUnit.MILLISECONDS)
            .readTimeout(10000, TimeUnit.MILLISECONDS)
            .build()

    @JvmStatic
    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder()
            .baseUrl("http://www.shop.ir/")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(StatableLiveListCallAdapterFactory())
            .build()

    @JvmStatic
    @Provides
    @Singleton
    fun provideShopService(retrofit: Retrofit): ShopService = retrofit.create(ShopService::class.java)
}