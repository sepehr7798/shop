package ir.bcoders.shop

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Provides
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

class ProjectViewModelFactory @Inject constructor(private val providerMap: MutableMap<Class<out ViewModel>, Provider<ViewModel>>) :
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        providerMap.forEach { entry ->
            if (modelClass.isAssignableFrom(entry.key)) {
                return entry.value.get() as T
            }
        }

        throw IllegalArgumentException("Invalid ViewModel class: $modelClass")
    }
}
