package ir.bcoders.shop.repository.network

import android.util.Log
import ir.bcoders.shop.utils.TAG
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import sb.liverecyclerview.StatableLiveList
import java.lang.reflect.Type

class StatableLiveListCallAdapter<R>(private val responseType: Type) :
    CallAdapter<List<R>, StatableLiveList<R>> {

    override fun adapt(call: Call<List<R>>): StatableLiveList<R> {
        val list = StatableLiveList<R>()
        list.setState(StatableLiveList.State.LOADING)
        call.enqueue(object : Callback<List<R>?> {
            override fun onFailure(call: Call<List<R>?>, t: Throwable) {
                list.setState(StatableLiveList.State.ERROR)
                Log.e(TAG, t.message, t)
            }

            override fun onResponse(call: Call<List<R>?>, response: Response<List<R>?>) {
                if (response.body() != null) {
                    list.setState(StatableLiveList.State.RESULT)
                    list.addAll(response.body()!!)
                } else {
                    list.setState(StatableLiveList.State.EMPTY)
                }
            }
        })

        return list
    }

    override fun responseType(): Type = responseType
}