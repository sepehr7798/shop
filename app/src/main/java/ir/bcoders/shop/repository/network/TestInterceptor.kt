package ir.bcoders.shop.repository.network

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import ir.bcoders.shop.*
import ir.bcoders.shop.di.ApplicationContext
import ir.bcoders.shop.models.HomeItem
import ir.bcoders.shop.utils.logD
import okhttp3.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.IllegalArgumentException
import javax.inject.Inject

class TestInterceptor @Inject constructor(
    @ApplicationContext private val context: Context
) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val path = chain.request().url().encodedPath()

        var rc = 0
        val body = when {
            path.contains("home_items") -> JsonArray().apply {
                listOf(
                    fakeCategories(rc++),
                    fakeOffer(rc++),
                    fakeProducts(rc++),
                    fakeProducts(rc++),
                    fakeOffer(rc++),
                    fakeProducts(rc++),
                    fakeOffer(rc++),
                    fakeProducts(rc++),
                    fakeProducts(rc)
                ).forEach {
                    add(JsonParser().parse(Gson().toJson(it)))
                }
            }.toString()
            path.contains("sub_categories") -> JsonArray().apply {
                List(20) { fakeSubCategory(it) }.forEach {
                    add(JsonParser().parse(Gson().toJson(it)))
                }
            }.toString()
            path.contains("categories") -> JsonArray().apply {
                List(20) { fakeCategory(it) }.forEach {
                    add(JsonParser().parse(Gson().toJson(it)))
                }
            }.toString()
            path.contains("home_products") -> JsonArray().apply {
                List(20) { fakeProduct(it) }.forEach {
                    add(JsonParser().parse(Gson().toJson(it)))
                }
            }.toString()
            path.contains("products") -> JsonArray().apply {
                List(100) { fakeProduct(it) }.forEach {
                    add(JsonParser().parse(Gson().toJson(it)))
                }
            }.toString()
            path.contains("most_useful_reviews") -> JsonArray().apply {
                List(2) { fakeReview(it) }.forEach {
                    add(JsonParser().parse(Gson().toJson(it)))
                }
            }.toString()
            path.contains("reviews") -> JsonArray().apply {
                List(50) { fakeReview(it) }.forEach {
                    add(JsonParser().parse(Gson().toJson(it)))
                }
            }.toString()
            path.contains("product_images") -> JsonArray().apply {
                repeat(5) {
                    add(randomImageUrl())
                }
            }.toString()
            else -> throw IllegalArgumentException("unknown request! : ${chain.request()}")
        }

        Thread.sleep(1000)

        return Response.Builder()
            .code(200)
            .addHeader("content-type", "application/json")
            .request(chain.request())
            .protocol(Protocol.HTTP_1_0)
            .body(ResponseBody.create(MediaType.parse("application/json"), body))
            .message(body)
            .build()
    }
}