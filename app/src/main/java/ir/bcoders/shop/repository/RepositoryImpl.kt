package ir.bcoders.shop.repository

import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonParser
import ir.bcoders.shop.models.*
import ir.bcoders.shop.repository.network.ShopService
import ir.bcoders.shop.utils.TAG
import ir.bcoders.shop.utils.logD
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import sb.livecollection.LiveList
import sb.liverecyclerview.StatableLiveList
import javax.inject.Inject

class RepositoryImpl(private val shopService: ShopService) : Repository {

    override fun getHomeItems(): StatableLiveList<HomeItem> {
        val sll = StatableLiveList<HomeItem>()
        sll.setState(StatableLiveList.State.LOADING)

        shopService.getHomeItems().enqueue(object : Callback<JsonArray?> {
            override fun onFailure(call: Call<JsonArray?>, t: Throwable) {
                sll.setState(StatableLiveList.State.ERROR)
                Log.e(TAG, t.message, t)
            }

            override fun onResponse(call: Call<JsonArray?>, response: Response<JsonArray?>) {
                if (response.body() == null || response.body()!!.size() == 0) {
                    sll.setState(StatableLiveList.State.EMPTY)
                    return
                }

                val list = mutableListOf<HomeItem>()
                response.body()!!.forEach {
                    list.add(
                        when (it.asJsonObject.get("type").asInt) {
                            HomeItem.TYPE_CATEGORIES -> Gson().fromJson(
                                it.toString(),
                                HomeItem.Categories::class.java
                            )

                            HomeItem.TYPE_PRODUCT_LIST -> Gson().fromJson(
                                it.toString(),
                                HomeItem.Products::class.java
                            )

                            HomeItem.TYPE_OFFER -> Gson().fromJson(
                                it.toString(),
                                HomeItem.Offer::class.java
                            )

                            else -> throw IllegalArgumentException("Invalid home item: $it")
                        }
                    )
                }

                sll.addAll(list)
                sll.setState(StatableLiveList.State.RESULT)
            }
        })

        return sll
    }

    override fun getCategories(): StatableLiveList<Category> {
        return shopService.getCategories()
    }

    override fun getSubCategories(categoryId: Int): StatableLiveList<SubCategory> {
        return shopService.getSubCategories(categoryId)
    }

    override fun getProducts(subCategoryId: Int): StatableLiveList<Product> {
        return shopService.getProducts(subCategoryId)
    }

    override fun getHomeProducts(homeItemId: Int): StatableLiveList<Product> {
        return shopService.getHomeProducts(homeItemId)
    }

    override fun getProductImageUrls(productId: Int): LiveList<String> {
        val list = LiveList<String>()
        shopService.getProductImageUrls(productId).enqueue(object : Callback<List<String>?> {
            override fun onFailure(call: Call<List<String>?>, t: Throwable) {
                Log.e(TAG, t.message, t)
            }

            override fun onResponse(call: Call<List<String>?>, response: Response<List<String>?>) {
                if (response.body() != null) list.addAll(response.body()!!).logD()
            }
        })

        return list
    }

    override fun getMostUsefulReviews(productId: Int): StatableLiveList<Review> {
        return shopService.getMostUsefulReviews(productId)
    }

    override fun getAllReviews(productId: Int): StatableLiveList<Review> {
        return shopService.getAllReviews(productId)
    }
}