package ir.bcoders.shop.repository.network

import ir.bcoders.shop.models.HomeItem
import retrofit2.CallAdapter
import retrofit2.Retrofit
import sb.liverecyclerview.StatableLiveList
import java.lang.reflect.Type

class StatableLiveListCallAdapterFactory : CallAdapter.Factory() {

    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        if (getRawType(returnType) != StatableLiveList::class.java) {
            return null
        }
        return StatableLiveListCallAdapter<Any>(returnType)
    }
}