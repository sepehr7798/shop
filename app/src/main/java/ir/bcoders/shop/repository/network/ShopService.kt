package ir.bcoders.shop.repository.network

import com.google.gson.JsonArray
import ir.bcoders.shop.models.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import sb.liverecyclerview.StatableLiveList

interface ShopService {

    @GET("home_items")
    fun getHomeItems(): Call<JsonArray>

    @GET("categories")
    fun getCategories(): StatableLiveList<Category>

    @GET("sub_categories")
    fun getSubCategories(@Query("id") categoryId: Int): StatableLiveList<SubCategory>

    @GET("products")
    fun getProducts(@Query("id") subCategoryId: Int): StatableLiveList<Product>

    @GET("home_products")
    fun getHomeProducts(@Query("id") homeItemId: Int): StatableLiveList<Product>

    @GET("product_images")
    fun getProductImageUrls(@Query("id") productId: Int): Call<List<String>>

    @GET("most_useful_reviews")
    fun getMostUsefulReviews(@Query("id") productId: Int): StatableLiveList<Review>

    @GET("reviews")
    fun getAllReviews(@Query("id") productId: Int): StatableLiveList<Review>
}