package ir.bcoders.shop.repository

import ir.bcoders.shop.models.*
import sb.livecollection.LiveList
import sb.liverecyclerview.StatableLiveList

interface Repository {

    fun getHomeItems(): StatableLiveList<HomeItem>

    fun getCategories(): StatableLiveList<Category>

    fun getSubCategories(categoryId: Int): StatableLiveList<SubCategory>

    fun getProducts(subCategoryId: Int): StatableLiveList<Product>

    fun getHomeProducts(homeItemId: Int): StatableLiveList<Product>

    fun getProductImageUrls(productId: Int): LiveList<String>

    fun getMostUsefulReviews(productId: Int): StatableLiveList<Review>

    fun getAllReviews(productId: Int): StatableLiveList<Review>
}