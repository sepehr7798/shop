package ir.bcoders.shop.adapters

import android.view.ViewGroup
import ir.bcoders.shop.R
import ir.bcoders.shop.models.SubCategory
import sb.liverecyclerview.adapters.BindingItemLiveListAdapter

class SubCategoriesAdapter(private val callback: Callback) :
    BindingItemLiveListAdapter<SubCategory>() {

    override fun getItemViewType(position: Int): Int = R.layout.item_sub_category

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): Holder<SubCategory> = super.onCreateViewHolder(parent, viewType).apply {
        itemView.setOnClickListener { callback.onSubCategoryClicked(getItem(adapterPosition)) }
    }


    interface Callback {
        fun onSubCategoryClicked(subCategory: SubCategory)
    }
}