package ir.bcoders.shop.adapters

import android.view.ViewGroup
import android.widget.Button
import ir.bcoders.shop.R
import ir.bcoders.shop.models.Review
import sb.liverecyclerview.adapters.BindingItemLiveListAdapter

class ReviewsAdapter(private val callback: Callback) : BindingItemLiveListAdapter<Review>() {

    override fun getItemViewType(position: Int): Int = R.layout.item_review

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder<Review> =
        super.onCreateViewHolder(parent, viewType).apply {
            itemView.findViewById<Button>(R.id.item_review_helpful_button)
                .setOnClickListener { callback.onUsefulButtonClicked(getItem(adapterPosition)) }
        }

    interface Callback {
        fun onUsefulButtonClicked(review: Review)
    }
}