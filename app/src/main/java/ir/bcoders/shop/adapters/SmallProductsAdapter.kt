package ir.bcoders.shop.adapters

import android.view.ViewGroup
import ir.bcoders.shop.R
import ir.bcoders.shop.models.Product
import sb.liverecyclerview.adapters.BindingItemLiveListAdapter

class SmallProductsAdapter(private val callback: Callback) : BindingItemLiveListAdapter<Product>() {

    override fun getItemViewType(position: Int): Int = R.layout.item_product_small

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder<Product> =
        super.onCreateViewHolder(parent, viewType).apply {
            itemView.setOnClickListener {
                callback.onProductClicked(getItem(adapterPosition))
            }
        }


    interface Callback {
        fun onProductClicked(product: Product)
    }
}