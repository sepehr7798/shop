package ir.bcoders.shop.adapters.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import ir.bcoders.shop.databinding.ItemProductListBinding
import ir.bcoders.shop.models.HomeItem
import ir.bcoders.shop.models.Product
import sb.liverecyclerview.StatableLiveList
import sb.liverecyclerview.StatableLiveRecyclerView

class HomeItemProductsViewHolder(private val binding: ItemProductListBinding) :
    RecyclerView.ViewHolder(binding.root) {

    val productsLiveRecyclerView: StatableLiveRecyclerView = binding.homeProductsLreeView
    val moreButton: View = binding.homeProductsMoreButton

    fun bind(
        productsItem: HomeItem.Products,
        products: StatableLiveList<Product>
    ) {
        binding.productsItem = productsItem
        productsLiveRecyclerView.setData(products)
    }
}