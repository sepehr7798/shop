package ir.bcoders.shop.adapters

import android.view.ViewGroup
import ir.bcoders.shop.R
import ir.bcoders.shop.models.Category
import sb.liverecyclerview.adapters.BindingItemLiveListAdapter

class CategoriesAdapter(private val callback: Callback) :
    BindingItemLiveListAdapter<Category>() {

    override fun getItemViewType(position: Int): Int = R.layout.item_category

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder<Category> =
        super.onCreateViewHolder(parent, viewType).apply {
            itemView.setOnClickListener {
                callback.onCategoryClicked(getItem(adapterPosition))
            }
        }

    interface Callback {
        fun onCategoryClicked(category: Category)
    }
}
