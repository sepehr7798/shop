package ir.bcoders.shop.adapters.holders

import androidx.recyclerview.widget.RecyclerView
import ir.bcoders.shop.databinding.ItemOfferBinding
import ir.bcoders.shop.models.HomeItem

class HomeItemOfferViewHolder(private val binding: ItemOfferBinding) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(offerItem: HomeItem.Offer) {
        binding.offerItem = offerItem
    }
}