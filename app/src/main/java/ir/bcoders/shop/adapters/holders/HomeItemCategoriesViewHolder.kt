package ir.bcoders.shop.adapters.holders

import androidx.recyclerview.widget.RecyclerView
import ir.bcoders.shop.databinding.ItemCategoryListBinding
import ir.bcoders.shop.models.Category
import sb.liverecyclerview.StatableLiveList
import sb.liverecyclerview.StatableLiveRecyclerView

class HomeItemCategoriesViewHolder(private val binding: ItemCategoryListBinding) :
    RecyclerView.ViewHolder(binding.root) {

    val categoriesLiveRecyclerView: StatableLiveRecyclerView = binding.homeCategoriesLreeView

    fun bind(categories: StatableLiveList<Category>) {
        categoriesLiveRecyclerView.setData(categories)
    }
}