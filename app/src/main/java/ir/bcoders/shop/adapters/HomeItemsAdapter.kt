package ir.bcoders.shop.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.bcoders.shop.R
import ir.bcoders.shop.adapters.holders.HomeItemCategoriesViewHolder
import ir.bcoders.shop.adapters.holders.HomeItemOfferViewHolder
import ir.bcoders.shop.adapters.holders.HomeItemProductsViewHolder
import ir.bcoders.shop.databinding.ItemCategoryListBinding
import ir.bcoders.shop.databinding.ItemOfferBinding
import ir.bcoders.shop.databinding.ItemProductListBinding
import ir.bcoders.shop.models.Category
import ir.bcoders.shop.models.HomeItem
import ir.bcoders.shop.models.Product
import ir.bcoders.shop.utils.addSpacingItemDecoration
import sb.liverecyclerview.StatableLiveList
import sb.liverecyclerview.adapters.LiveListAdapter

class HomeItemsAdapter(private val callback: Callback, private val dataHelper: DataHelper) :
    LiveListAdapter<HomeItem, RecyclerView.ViewHolder>() {

    private val productsViewPool: RecyclerView.RecycledViewPool = RecyclerView.RecycledViewPool()

    override fun getItemViewType(position: Int): Int = when (getItem(position)) {
        is HomeItem.Categories -> R.layout.item_category_list
        is HomeItem.Products -> R.layout.item_product_list
        is HomeItem.Offer -> R.layout.item_offer
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder = when (viewType) {
        R.layout.item_category_list ->
            HomeItemCategoriesViewHolder(
                ItemCategoryListBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            ).apply {
                categoriesLiveRecyclerView.setHasFixedSize(true)
                categoriesLiveRecyclerView.adapter = CategoriesAdapter(callback)
                categoriesLiveRecyclerView.addSpacingItemDecoration(
                    parent.context.resources.getDimension(R.dimen.activity_horizontal_gutter).toInt(),
                    startMargin = parent.context.resources.getDimension(R.dimen.activity_horizontal_margin).toInt(),
                    endMargin = parent.context.resources.getDimension(R.dimen.activity_horizontal_margin).toInt()
                )
            }
        R.layout.item_product_list ->
            HomeItemProductsViewHolder(
                ItemProductListBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            ).apply {
                productsLiveRecyclerView.setHasFixedSize(true)
                productsLiveRecyclerView.adapter = SmallProductsAdapter(callback)
                productsLiveRecyclerView.setRecycledViewPool(productsViewPool)
                productsLiveRecyclerView.addSpacingItemDecoration(
                    parent.context.resources.getDimension(R.dimen.activity_horizontal_gutter).toInt(),
                    startMargin = parent.context.resources.getDimension(R.dimen.activity_horizontal_margin).toInt(),
                    endMargin = parent.context.resources.getDimension(R.dimen.activity_horizontal_margin).toInt()
                )

                moreButton.setOnClickListener {
                    callback.onMoreButtonClicked(getItem(adapterPosition) as HomeItem.Products)
                }
            }
        R.layout.item_offer ->
            HomeItemOfferViewHolder(
                ItemOfferBinding.inflate(
                    LayoutInflater.from(parent.context), parent, false
                )
            )
        else -> throw IllegalArgumentException("invalid viewType: $viewType")
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (getItem(position)) {
            is HomeItem.Categories -> {
                (holder as HomeItemCategoriesViewHolder).bind(
                    dataHelper.getCategories(getItem(position) as HomeItem.Categories)
                )
            }
            is HomeItem.Products -> {
                (holder as HomeItemProductsViewHolder).bind(
                    getItem(position) as HomeItem.Products,
                    dataHelper .getProducts(getItem(position) as HomeItem.Products)
                )
            }
            is HomeItem.Offer -> {
                (holder as HomeItemOfferViewHolder).bind(getItem(position) as HomeItem.Offer)
            }
        }
    }


    interface Callback : SmallProductsAdapter.Callback, CategoriesAdapter.Callback {

        fun onMoreButtonClicked(homeItemProducts: HomeItem.Products)
    }

    interface DataHelper {
        fun getCategories(homeItemCategories: HomeItem.Categories): StatableLiveList<Category>

        fun getProducts(homeItemProducts: HomeItem.Products): StatableLiveList<Product>
    }
}

