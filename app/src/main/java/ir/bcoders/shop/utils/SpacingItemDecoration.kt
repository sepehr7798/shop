package ir.bcoders.shop.utils

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class SpacingItemDecoration(
    private val spaceInPixels: Int,
    private val startMargin: Int = 0,
    private val endMargin: Int = 0,
    private val topMargin: Int = 0,
    private val bottomMargin: Int = 0
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        val position = parent.getChildAdapterPosition(view)

        val layoutManager: RecyclerView.LayoutManager = parent.layoutManager!!
        val isVertical: Boolean
        if (layoutManager is GridLayoutManager) {
            throw RuntimeException("GridLayoutManager is not supported")
        } else if (layoutManager is LinearLayoutManager) {
            isVertical = layoutManager.orientation == RecyclerView.VERTICAL

            if (isVertical) {
                outRect.top = spaceInPixels / 2
                outRect.bottom = spaceInPixels / 2
            } else {
                outRect.left = spaceInPixels / 2
                outRect.right = spaceInPixels / 2
            }

            if (position == 0) {
                if (isVertical) {
                    outRect.top = topMargin
                } else {
                    if (isRtl(parent)) outRect.right = startMargin
                    else outRect.left = startMargin
                }
            }
            if (position == itemCount(parent) - 1) {
                if (isVertical) {
                    outRect.bottom = bottomMargin
                } else {
                    if (isRtl(parent)) {
                        outRect.left = endMargin
                    } else {
                        outRect.right = endMargin
                    }
                }
            }
        } else {
            throw IllegalArgumentException("Invalid layout manager")
        }
    }

    private fun itemCount(parent: RecyclerView): Int = parent.adapter?.itemCount ?: 0

    private fun isRtl(parent: RecyclerView) = parent.layoutDirection == View.LAYOUT_DIRECTION_RTL
}

fun RecyclerView.addSpacingItemDecoration(
    spaceInPixels: Int,
    startMargin: Int = 0,
    endMargin: Int = 0,
    topMargin: Int = 0,
    bottomMargin: Int = 0
) {
    addItemDecoration(
        SpacingItemDecoration(
            spaceInPixels,
            startMargin,
            endMargin,
            topMargin,
            bottomMargin
        )
    )
}
