package ir.bcoders.shop.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.squareup.picasso.Callback
import com.squareup.picasso.NetworkPolicy
import com.squareup.picasso.Picasso
import ir.bcoders.shop.R

object BindingAdapters {

    @JvmStatic
    @BindingAdapter("app:picasso_url")
    fun loadUrl(imageView: ImageView, url: String?) {
        if (url == null) {
            imageView.setImageResource(R.mipmap.ic_launcher)
            return
        }
        
        val imageSize = 400

        Picasso.get()
            .load(url)
            .networkPolicy(NetworkPolicy.OFFLINE)
            .resize(imageSize, imageSize)
            .centerCrop()
            .into(imageView, object : Callback {
                override fun onSuccess() {}

                override fun onError(e: Exception?) {
                    Picasso.get()
                        .load(url)
                        .resize(imageSize, imageSize)
                        .centerCrop()
                        .into(imageView)
                }
            })
    }
}