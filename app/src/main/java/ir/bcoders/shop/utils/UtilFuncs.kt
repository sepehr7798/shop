package ir.bcoders.shop.utils

import android.content.Context
import android.util.DisplayMetrics
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import sb.liverecyclerview.StatableLiveList
import kotlin.coroutines.CoroutineContext

fun convertDpToPixel(context: Context, dp: Float): Int =
    (dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()

fun <T> coroutineLoader(
    scope: CoroutineScope,
    context: CoroutineContext = Dispatchers.Main,
    loader: suspend () -> List<T>
): StatableLiveList<T> {
    val liveList = StatableLiveList<T>()
    scope.launch(context) {
        liveList.setState(StatableLiveList.State.LOADING)
        val list = loader.invoke()
        liveList.load { list }
    }

    return liveList
}

fun <T : Any?> T.logD(
    tag: String = this.TAG
): T {
    Log.d(tag, this.toString())
    return this
}

val Any?.TAG: String
    get() = if (this is Any) this.javaClass.simpleName else "Null Reference"