package ir.bcoders.shop.activities

interface MainActivityBaseCallback {

    fun setTitle(title: String)

    fun setIsAppbarSelected(isSelected: Boolean)

    fun setHomeAsUpEnabled(isEnabled: Boolean)
}