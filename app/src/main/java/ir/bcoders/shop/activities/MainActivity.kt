package ir.bcoders.shop.activities

import android.content.Context
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit
import ir.bcoders.shop.R
import ir.bcoders.shop.fragments.*
import ir.bcoders.shop.iUrl
import ir.bcoders.shop.models.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),
    HomeFragment.Callback,
    SubCategoriesFragment.Callback,
    ProductsFragment.Callback,
    ProductDetailsFragment.Callback,
    ReviewsFragment.Callback,
    PhotoViewFragment.Callback {

    private lateinit var homeFragment: HomeFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        //TODO: Test
        iUrl = getPreferences(Context.MODE_PRIVATE).getString("ASDF", "https://www.digikala.com")!!

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        if (savedInstanceState == null) {
            homeFragment = HomeFragment.newInstance()
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, homeFragment)
                .commit()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            R.id.action_settings -> {
                //TODO: Test
                val editText = EditText(this)
                AlertDialog.Builder(this)
                    .setView(editText as View)
                    .setPositiveButton("OK") { _, _ ->
                        getPreferences(Context.MODE_PRIVATE)
                            .edit {
                                putString("ASDF", editText.text.toString())
                            }
                    }.show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun setTitle(title: String) {
        this.title = title
    }

    override fun setIsAppbarSelected(isSelected: Boolean) {
        appbar.isSelected = isSelected
    }

    override fun setHomeAsUpEnabled(isEnabled: Boolean) {
        supportActionBar?.setDisplayHomeAsUpEnabled(isEnabled)
    }

    override fun onMoreButtonClicked(homeItemProducts: HomeItem.Products) {
        supportFragmentManager.beginTransaction()
            .replace(
                R.id.container,
                ProductsFragment.newInstance(
                    SubCategory(
                        homeItemProducts.id,
                        homeItemProducts.title,
                        "",
                        "",
                        0
                    )
                )
            )
            .addToBackStack(null)
            .commit()
    }

    override fun onProductClicked(product: Product) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, ProductDetailsFragment.newInstance(product))
            .addToBackStack(null)
            .commit()
    }

    override fun onCategoryClicked(category: Category) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, SubCategoriesFragment.newInstance(category))
            .addToBackStack(null)
            .commit()
    }

    override fun onSubCategoryClicked(subCategory: SubCategory) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, ProductsFragment.newInstance(subCategory))
            .addToBackStack(null)
            .commit()
    }

    override fun onShowAllReviewsClicked(product: Product) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, ReviewsFragment.newInstance(product))
            .addToBackStack(null)
            .commit()
    }

    override fun onImageClicked(imageUrl: String) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, PhotoViewFragment.newInstance(imageUrl))
            .addToBackStack(null)
            .commit()
    }

    override fun onAddToCartButtonClicked(product: Product) {
        TODO()
    }

    override fun onUsefulButtonClicked(review: Review) {
        TODO()
    }
}
