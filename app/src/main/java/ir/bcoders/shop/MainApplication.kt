package ir.bcoders.shop

import android.app.Application
import android.app.IntentService
import android.util.Log
import com.squareup.picasso.Picasso
import ir.bcoders.shop.di.components.ApplicationComponent
import ir.bcoders.shop.di.components.DaggerApplicationComponent
import ir.bcoders.shop.repository.network.ShopService
import javax.inject.Inject

class MainApplication : Application() {

    @Inject
    lateinit var picasso: Picasso

    @Inject
    lateinit var shopService: ShopService

    override fun onCreate() {
        super.onCreate()
        component = DaggerApplicationComponent.builder()
            .applicationContext(applicationContext)
            .build()
        component.inject(this)

        Picasso.setSingletonInstance(picasso)
    }


    companion object {
        private lateinit var component: ApplicationComponent

        fun getApplicationComponent(): ApplicationComponent {
            if (!::component.isInitialized)
                throw RuntimeException("Application component has not been initialized yet!")

            return component
        }
    }
}