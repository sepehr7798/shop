package ir.bcoders.shop.viewmodels

import androidx.lifecycle.ViewModel
import ir.bcoders.shop.models.Product
import ir.bcoders.shop.models.SubCategory
import sb.liverecyclerview.StatableLiveList

abstract class ProductsViewModel : ViewModel() {

    lateinit var subCategory: SubCategory

    abstract val productList: StatableLiveList<Product>
}