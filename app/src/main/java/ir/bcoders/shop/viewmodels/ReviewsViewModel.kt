package ir.bcoders.shop.viewmodels

import androidx.lifecycle.ViewModel
import ir.bcoders.shop.models.Product
import ir.bcoders.shop.models.Review
import sb.liverecyclerview.StatableLiveList

abstract class ReviewsViewModel: ViewModel() {

    lateinit var product: Product

    abstract val reviews: StatableLiveList<Review>
}