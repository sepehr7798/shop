package ir.bcoders.shop.viewmodels.impls

import android.util.Log
import ir.bcoders.shop.models.Category
import ir.bcoders.shop.models.HomeItem
import ir.bcoders.shop.models.Product
import ir.bcoders.shop.repository.Repository
import ir.bcoders.shop.utils.logD
import ir.bcoders.shop.viewmodels.HomeViewModel
import sb.liverecyclerview.StatableLiveList
import javax.inject.Inject

class HomeViewModelImpl @Inject constructor(private val repository: Repository) : HomeViewModel() {

    override val homeItems: StatableLiveList<HomeItem> by lazy {
        repository.getHomeItems()
    }

    override fun getCategories(homeItemCategories: HomeItem.Categories): StatableLiveList<Category> {
        return repository.getCategories()
    }

    override fun getProducts(homeItemProducts: HomeItem.Products): StatableLiveList<Product> {
        return repository.getHomeProducts(homeItemProducts.id)
    }
}