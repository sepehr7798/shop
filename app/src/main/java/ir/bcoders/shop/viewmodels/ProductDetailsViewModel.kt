package ir.bcoders.shop.viewmodels

import androidx.lifecycle.ViewModel
import ir.bcoders.shop.models.Product
import ir.bcoders.shop.models.Review
import sb.livecollection.LiveList
import sb.liverecyclerview.StatableLiveList

abstract class ProductDetailsViewModel : ViewModel() {

    lateinit var product: Product

    abstract val imagesUrl: LiveList<String>

    abstract val mostUsefulReviews: StatableLiveList<Review>
}