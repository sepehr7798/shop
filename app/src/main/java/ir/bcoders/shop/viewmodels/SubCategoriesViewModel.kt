package ir.bcoders.shop.viewmodels

import androidx.lifecycle.ViewModel
import ir.bcoders.shop.models.Category
import ir.bcoders.shop.models.SubCategory
import sb.liverecyclerview.StatableLiveList

abstract class SubCategoriesViewModel : ViewModel() {

    lateinit var category: Category

    abstract val subCategories: StatableLiveList<SubCategory>
}