package ir.bcoders.shop.viewmodels.testimpls

import androidx.lifecycle.viewModelScope
import ir.bcoders.shop.fakeProduct
import ir.bcoders.shop.models.Product
import ir.bcoders.shop.utils.coroutineLoader
import ir.bcoders.shop.viewmodels.ProductsViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import sb.liverecyclerview.StatableLiveList
import javax.inject.Inject

class ProductsViewModelTestImpl @Inject constructor() : ProductsViewModel() {

    override val productList: StatableLiveList<Product> by lazy {
        coroutineLoader<Product>(viewModelScope, Dispatchers.IO) {
            delay(1000)

            var i = 0
            return@coroutineLoader listOf(
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i++),
                fakeProduct(i)
            )
        }
    }
}