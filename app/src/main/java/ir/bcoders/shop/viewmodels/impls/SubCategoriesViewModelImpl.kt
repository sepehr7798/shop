package ir.bcoders.shop.viewmodels.impls

import ir.bcoders.shop.models.SubCategory
import ir.bcoders.shop.repository.Repository
import ir.bcoders.shop.viewmodels.SubCategoriesViewModel
import sb.liverecyclerview.StatableLiveList
import javax.inject.Inject

class SubCategoriesViewModelImpl
@Inject constructor(private val repository: Repository) : SubCategoriesViewModel() {

    override val subCategories: StatableLiveList<SubCategory> by lazy {
        repository.getSubCategories(category.id)
    }
}