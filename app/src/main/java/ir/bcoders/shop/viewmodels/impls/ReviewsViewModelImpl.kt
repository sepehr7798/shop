package ir.bcoders.shop.viewmodels.impls

import ir.bcoders.shop.models.Review
import ir.bcoders.shop.repository.Repository
import ir.bcoders.shop.viewmodels.ReviewsViewModel
import sb.liverecyclerview.StatableLiveList
import javax.inject.Inject

class ReviewsViewModelImpl
@Inject constructor(private val repository: Repository) : ReviewsViewModel() {

    override val reviews: StatableLiveList<Review> by lazy { repository.getAllReviews(product.id) }
}