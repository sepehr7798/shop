package ir.bcoders.shop.viewmodels

import androidx.lifecycle.ViewModel
import ir.bcoders.shop.models.Category
import ir.bcoders.shop.models.HomeItem
import ir.bcoders.shop.models.Product
import sb.liverecyclerview.StatableLiveList

abstract class HomeViewModel : ViewModel() {

    abstract val homeItems: StatableLiveList<HomeItem>

    abstract fun getCategories(homeItemCategories: HomeItem.Categories): StatableLiveList<Category>

    abstract fun getProducts(homeItemProducts: HomeItem.Products): StatableLiveList<Product>
}