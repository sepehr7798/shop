package ir.bcoders.shop.viewmodels.testimpls

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import ir.bcoders.shop.fakeSubCategory
import ir.bcoders.shop.models.Category
import ir.bcoders.shop.models.SubCategory
import ir.bcoders.shop.utils.coroutineLoader
import ir.bcoders.shop.viewmodels.SubCategoriesViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import sb.liverecyclerview.StatableLiveList
import javax.inject.Inject

class SubCategoriesViewModelTestImpl @Inject constructor() : SubCategoriesViewModel() {

    override val subCategories: StatableLiveList<SubCategory> by lazy {
        coroutineLoader<SubCategory>(viewModelScope, Dispatchers.IO) {
            delay(1000)

            var i = 0
            return@coroutineLoader listOf(
                fakeSubCategory(i++),
                fakeSubCategory(i++),
                fakeSubCategory(i++),
                fakeSubCategory(i++),
                fakeSubCategory(i++),
                fakeSubCategory(i++),
                fakeSubCategory(i++),
                fakeSubCategory(i++),
                fakeSubCategory(i++),
                fakeSubCategory(i++),
                fakeSubCategory(i++),
                fakeSubCategory(i)
            )
        }
    }
}