package ir.bcoders.shop.viewmodels.impls

import ir.bcoders.shop.models.Review
import ir.bcoders.shop.repository.Repository
import ir.bcoders.shop.viewmodels.ProductDetailsViewModel
import sb.livecollection.LiveList
import sb.liverecyclerview.StatableLiveList
import javax.inject.Inject

class ProductDetailsViewModelImpl @Inject constructor(repository: Repository) :
    ProductDetailsViewModel() {

    override val imagesUrl: LiveList<String> by lazy { repository.getProductImageUrls(product.id) }

    override val mostUsefulReviews: StatableLiveList<Review> by lazy {
        repository.getMostUsefulReviews(product.id)
    }
}