package ir.bcoders.shop.viewmodels.testimpls

import androidx.lifecycle.viewModelScope
import ir.bcoders.shop.fakeReview
import ir.bcoders.shop.models.Review
import ir.bcoders.shop.utils.coroutineLoader
import ir.bcoders.shop.viewmodels.ReviewsViewModel
import kotlinx.coroutines.delay
import sb.liverecyclerview.StatableLiveList
import java.util.Collections.addAll
import javax.inject.Inject

class ReviewsViewModelTestImpl @Inject constructor() : ReviewsViewModel() {

    override val reviews: StatableLiveList<Review> by lazy {
        coroutineLoader<Review>(viewModelScope) {
            delay(1000)

            var i = 0
            return@coroutineLoader listOf(
                fakeReview(i++),
                fakeReview(i++),
                fakeReview(i++),
                fakeReview(i++),
                fakeReview(i++),
                fakeReview(i++),
                fakeReview(i++),
                fakeReview(i++),
                fakeReview(i++),
                fakeReview(i++),
                fakeReview(i++),
                fakeReview(i++),
                fakeReview(i++),
                fakeReview(i++),
                fakeReview(i++),
                fakeReview(i)
            )
        }
    }
}