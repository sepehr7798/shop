package ir.bcoders.shop.viewmodels.testimpls

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import ir.bcoders.shop.fakeReview
import ir.bcoders.shop.models.Product
import ir.bcoders.shop.models.Review
import ir.bcoders.shop.randomImageUrl
import ir.bcoders.shop.utils.coroutineLoader
import ir.bcoders.shop.viewmodels.ProductDetailsViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import sb.livecollection.LiveList
import sb.liverecyclerview.StatableLiveList
import javax.inject.Inject

class ProductDetailsViewModelTestImpl @Inject constructor() : ProductDetailsViewModel() {

    override val imagesUrl: LiveList<String> by lazy {
        val mutableList = LiveList<String>()
        viewModelScope.launch {
            delay(1000)
            mutableList.addAll(
                listOf(
                    randomImageUrl(),
                    randomImageUrl(),
                    randomImageUrl(),
                    randomImageUrl(),
                    randomImageUrl(),
                    randomImageUrl(),
                    randomImageUrl()
                )
            )
        }

        return@lazy mutableList
    }

    override val mostUsefulReviews: StatableLiveList<Review> by lazy {
        coroutineLoader<Review>(viewModelScope) {
            delay(1000)

            var i = 0
            return@coroutineLoader listOf(
                fakeReview(i++),
                fakeReview(i)
            )
        }
    }
}