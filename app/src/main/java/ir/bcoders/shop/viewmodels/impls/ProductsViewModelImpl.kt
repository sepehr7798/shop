package ir.bcoders.shop.viewmodels.impls

import ir.bcoders.shop.models.Product
import ir.bcoders.shop.repository.Repository
import ir.bcoders.shop.viewmodels.ProductsViewModel
import sb.liverecyclerview.StatableLiveList
import javax.inject.Inject

class ProductsViewModelImpl
@Inject constructor(private val repository: Repository) : ProductsViewModel() {

    override val productList: StatableLiveList<Product> by lazy { repository.getProducts(subCategory.id) }
}