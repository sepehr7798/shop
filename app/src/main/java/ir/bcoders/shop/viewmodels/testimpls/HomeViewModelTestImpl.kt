package ir.bcoders.shop.viewmodels.testimpls

import androidx.lifecycle.viewModelScope
import ir.bcoders.shop.*
import ir.bcoders.shop.models.Category
import ir.bcoders.shop.models.HomeItem
import ir.bcoders.shop.models.Product
import ir.bcoders.shop.utils.coroutineLoader
import ir.bcoders.shop.viewmodels.HomeViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import sb.liverecyclerview.StatableLiveList
import javax.inject.Inject

class HomeViewModelTestImpl @Inject constructor() : HomeViewModel() {

    override val homeItems: StatableLiveList<HomeItem> by lazy {
        coroutineLoader(viewModelScope, Dispatchers.IO) {
            var i = 0
            return@coroutineLoader listOf(
                fakeCategories(i++),
                fakeOffer(i++),
                fakeProducts(i++),
                fakeProducts(i++),
                fakeOffer(i++),
                fakeProducts(i++),
                fakeOffer(i++),
                fakeProducts(i)
            )
        }
    }

    override fun getCategories(homeItemCategories: HomeItem.Categories): StatableLiveList<Category> =
        coroutineLoader(viewModelScope, Dispatchers.IO) {
            delay(1000)
            return@coroutineLoader listOf(
                fakeCategory(0),
                fakeCategory(1),
                fakeCategory(2),
                fakeCategory(3),
                fakeCategory(4),
                fakeCategory(5),
                fakeCategory(6),
                fakeCategory(7),
                fakeCategory(8)
            )
        }

    override fun getProducts(homeItemProducts: HomeItem.Products): StatableLiveList<Product> =
        coroutineLoader(viewModelScope, Dispatchers.IO) {
            delay(1000)
            return@coroutineLoader mutableListOf<Product>().apply {
                repeat(15) {
                    add(fakeProduct(it))
                }
            }
        }
}