package ir.bcoders.shop.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import ir.bcoders.shop.MainApplication
import ir.bcoders.shop.R
import ir.bcoders.shop.activities.MainActivityBaseCallback
import ir.bcoders.shop.adapters.WideProductsAdapter
import ir.bcoders.shop.di.components.DaggerProductsComponent
import ir.bcoders.shop.models.SubCategory
import ir.bcoders.shop.utils.addSpacingItemDecoration
import ir.bcoders.shop.utils.convertDpToPixel
import ir.bcoders.shop.viewmodels.ProductsViewModel
import kotlinx.android.synthetic.main.fragment_products.*
import javax.inject.Inject

class ProductsFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var subCategory: SubCategory

    private lateinit var productsViewModel: ProductsViewModel

    private lateinit var callback: Callback

    override fun onAttach(context: Context) {
        super.onAttach(context)
        DaggerProductsComponent.builder()
            .applicationComponent(MainApplication.getApplicationComponent())
            .build()
            .inject(this)

        subCategory = arguments!!.getParcelable(ARG_SUB_CATEGORY)!!

        productsViewModel =
            ViewModelProvider(this, viewModelFactory).get(ProductsViewModel::class.java)
        productsViewModel.subCategory = subCategory

        callback = activity as Callback
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_products, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        callback.setIsAppbarSelected(false)

        products_lree_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                callback.setIsAppbarSelected(recyclerView.canScrollVertically(-1))
            }
        })

        products_lree_view.run {
            addSpacingItemDecoration(
                convertDpToPixel(context, 16F),
                topMargin = convertDpToPixel(context, 16F),
                bottomMargin = convertDpToPixel(context, 16F)
            )

            adapter = WideProductsAdapter(callback)
            setData(productsViewModel.productList)
        }
    }

    override fun onResume() {
        super.onResume()
        callback.setTitle(subCategory.name)
        callback.setHomeAsUpEnabled(true)
    }


    interface Callback : MainActivityBaseCallback, WideProductsAdapter.Callback


    companion object {

        private const val ARG_SUB_CATEGORY = "ARG_SUB_CATEGORY"

        fun newInstance(subCategory: SubCategory): ProductsFragment = ProductsFragment().apply {
            arguments = bundleOf(ARG_SUB_CATEGORY to subCategory)
        }
    }
}