package ir.bcoders.shop.fragments

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ir.bcoders.shop.MainApplication
import ir.bcoders.shop.R
import ir.bcoders.shop.activities.MainActivityBaseCallback
import ir.bcoders.shop.adapters.ReviewsAdapter
import ir.bcoders.shop.databinding.FragmentReviewsBinding
import ir.bcoders.shop.di.components.DaggerReviewsComponent
import ir.bcoders.shop.models.Product
import ir.bcoders.shop.utils.addSpacingItemDecoration
import ir.bcoders.shop.utils.convertDpToPixel
import ir.bcoders.shop.viewmodels.ReviewsViewModel
import kotlinx.android.synthetic.main.fragment_reviews.*
import javax.inject.Inject

class ReviewsFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var product: Product

    private lateinit var viewModel: ReviewsViewModel

    private lateinit var viewBinding: FragmentReviewsBinding

    private lateinit var callback: Callback

    override fun onAttach(context: Context) {
        super.onAttach(context)
        DaggerReviewsComponent.builder()
            .applicationComponet(MainApplication.getApplicationComponent())
            .build()
            .inject(this)

        product = arguments!!.getParcelable(ARG_PRODUCT)!!

        viewModel = ViewModelProvider(this, viewModelFactory).get(ReviewsViewModel::class.java)
        viewModel.product = product

        callback = activity as Callback
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_reviews, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        callback.setIsAppbarSelected(false)

        viewBinding = FragmentReviewsBinding.bind(view)
        viewBinding.product = product

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            fragment_reviews_nested_scroll_view.setOnScrollChangeListener { v, _, _, _, _ ->
                callback.setIsAppbarSelected(v.canScrollVertically(-1))
            }
        }

        fragment_reviews_lree_view.run {
            addSpacingItemDecoration(
                convertDpToPixel(context, 24F)
            )

            adapter = ReviewsAdapter(callback)
            setData(viewModel.reviews)
        }
    }

    override fun onResume() {
        super.onResume()
        callback.setHomeAsUpEnabled(true)
        callback.setTitle(getString(R.string.reviews))
    }


    interface Callback : MainActivityBaseCallback, ReviewsAdapter.Callback


    companion object {
        private const val ARG_PRODUCT = "ARG_PRODUCT"

        fun newInstance(product: Product): ReviewsFragment = ReviewsFragment().apply {
            arguments = bundleOf(ARG_PRODUCT to product)
        }
    }
}