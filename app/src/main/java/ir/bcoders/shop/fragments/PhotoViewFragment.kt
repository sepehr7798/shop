package ir.bcoders.shop.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.math.MathUtils
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import com.github.chrisbanes.photoview.PhotoView
import com.squareup.picasso.Picasso
import ir.bcoders.shop.R
import ir.bcoders.shop.activities.MainActivityBaseCallback
import ir.bcoders.shop.utils.BindingAdapters
import ir.bcoders.shop.utils.logD
import kotlin.math.abs
import kotlin.math.atan

class PhotoViewFragment : Fragment() {

    private lateinit var callback: Callback

    override fun onAttach(context: Context) {
        super.onAttach(context)

        callback = activity as Callback
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_photo_view, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Picasso.get()
            .load(arguments!!.getString(ARG_IMAGE_URL))
            .into(view.findViewById<PhotoView>(R.id.fragment_photo_view_photo_view))

        view.setOnTouchListener(PhotoViewTouchHandler { activity!!.onBackPressed() })
    }


    class PhotoViewTouchHandler(private val exit: () -> Unit) : View.OnTouchListener {

        private var startX: Float = -1F
        private var startY: Float = -1F

        override fun onTouch(v: View?, event: MotionEvent?): Boolean {
            when (event!!.action) {
                MotionEvent.ACTION_DOWN -> {
                    startX = event.x
                    startY = event.y
                }

                MotionEvent.ACTION_MOVE -> {
                    val dy = event.y - startY
                    v!!.y = dy
                }

                MotionEvent.ACTION_UP -> {
                    val dy = event.y - startY

                    if (abs(dy) > v!!.context.resources.displayMetrics.heightPixels / 6) {
                        exit.invoke()
                    } else {
                        v.y = 0F
                    }
                }
            }

            return true
        }
    }


    interface Callback : MainActivityBaseCallback


    companion object {

        private const val ARG_IMAGE_URL = "ARG_IMAGE_URL"

        fun newInstance(imageUrl: String): PhotoViewFragment = PhotoViewFragment().apply {
            arguments = bundleOf(
                ARG_IMAGE_URL to imageUrl
            )
        }
    }
}