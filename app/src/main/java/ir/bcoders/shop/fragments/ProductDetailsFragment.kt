package ir.bcoders.shop.fragments

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.PagerAdapter
import ir.bcoders.shop.MainApplication
import ir.bcoders.shop.R
import ir.bcoders.shop.activities.MainActivityBaseCallback
import ir.bcoders.shop.adapters.ReviewsAdapter
import ir.bcoders.shop.databinding.FragmentProductDetailsBinding
import ir.bcoders.shop.di.components.DaggerProductDetailsComponent
import ir.bcoders.shop.models.Product
import ir.bcoders.shop.utils.BindingAdapters
import ir.bcoders.shop.utils.addSpacingItemDecoration
import ir.bcoders.shop.utils.convertDpToPixel
import ir.bcoders.shop.viewmodels.ProductDetailsViewModel
import kotlinx.android.synthetic.main.fragment_product_details.*
import javax.inject.Inject

class ProductDetailsFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var product: Product

    private lateinit var viewModel: ProductDetailsViewModel

    private lateinit var viewBinding: FragmentProductDetailsBinding

    private lateinit var callback: Callback

    override fun onAttach(context: Context) {
        super.onAttach(context)
        DaggerProductDetailsComponent.builder()
            .applicationComponent(MainApplication.getApplicationComponent())
            .build()
            .inject(this)

        product = arguments!!.getParcelable(ARG_PRODUCT)!!

        viewModel =
            ViewModelProvider(this, viewModelFactory).get(ProductDetailsViewModel::class.java)
        viewModel.product = product

        callback = activity as Callback
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_product_details, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        callback.setIsAppbarSelected(false)

        viewBinding = FragmentProductDetailsBinding.bind(view)
        viewBinding.product = product

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            product_details_nested_scroll_view.setOnScrollChangeListener { v, _, _, _, _ ->
                val b = v.canScrollVertically(-1)
                callback.setIsAppbarSelected(b)
            }
        }

        viewModel.imagesUrl.observe(this, object : Observer<List<String>> {
            override fun onChanged(t: List<String>?) {
                if (t != null)
                    product_details_images_view_pager.adapter =
                        ImagesViewPagerAdapter(context!!, callback, t)
            }
        })

        product_details_most_helpful_reviews_lree_view.run {
            addSpacingItemDecoration(
                convertDpToPixel(context, 24F)
            )

            adapter = ReviewsAdapter(callback)
            setData(viewModel.mostUsefulReviews)
        }

        product_details_add_to_cart_button.setOnClickListener {
            callback.onAddToCartButtonClicked(product)
        }

        product_details_show_all_reviews_button.setOnClickListener {
            callback.onShowAllReviewsClicked(product)
        }
    }

    override fun onResume() {
        super.onResume()
        callback.setHomeAsUpEnabled(true)
        callback.setTitle("")
    }


    class ImagesViewPagerAdapter(
        private val context: Context,
        private val callback: Callback,
        private val imageUrls: List<String>
    ) : PagerAdapter() {

        override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

        override fun getCount(): Int = imageUrls.size

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val imageView = ImageView(context)
            BindingAdapters.loadUrl(imageView, imageUrls[position])
            imageView.setOnClickListener { callback.onImageClicked(imageUrls[position]) }
            container.addView(imageView)

            return imageView
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }
    }


    interface Callback : MainActivityBaseCallback, ReviewsAdapter.Callback {

        fun onImageClicked(imageUrl: String)

        fun onAddToCartButtonClicked(product: Product)

        fun onShowAllReviewsClicked(product: Product)
    }


    companion object {
        private const val ARG_PRODUCT = "ARG_PRODUCT"

        fun newInstance(product: Product): ProductDetailsFragment = ProductDetailsFragment().apply {
            arguments = bundleOf(ARG_PRODUCT to product)
        }
    }
}