package ir.bcoders.shop.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import ir.bcoders.shop.MainApplication
import ir.bcoders.shop.R
import ir.bcoders.shop.activities.MainActivityBaseCallback
import ir.bcoders.shop.adapters.SubCategoriesAdapter
import ir.bcoders.shop.di.components.DaggerSubCategoriesComponent
import ir.bcoders.shop.models.Category
import ir.bcoders.shop.utils.addSpacingItemDecoration
import ir.bcoders.shop.utils.convertDpToPixel
import ir.bcoders.shop.viewmodels.SubCategoriesViewModel
import kotlinx.android.synthetic.main.fragment_sub_categories.*
import javax.inject.Inject

class SubCategoriesFragment : Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var category: Category

    private lateinit var subCategoriesViewModel: SubCategoriesViewModel

    private lateinit var callback: Callback

    override fun onAttach(context: Context) {
        super.onAttach(context)
        DaggerSubCategoriesComponent.builder()
            .applicationComponent(MainApplication.getApplicationComponent())
            .build()
            .inject(this)

        category = arguments!!.getParcelable(ARG_CATEGORY)!!
        subCategoriesViewModel =
            ViewModelProvider(this, viewModelFactory).get(SubCategoriesViewModel::class.java)
        subCategoriesViewModel.category = category

        callback = activity as Callback
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_sub_categories, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        callback.setIsAppbarSelected(false)

        sub_categories_lree_view.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                callback.setIsAppbarSelected(recyclerView.canScrollVertically(-1))
            }
        })

        sub_categories_lree_view.run {
            addSpacingItemDecoration(
                convertDpToPixel(context, 16F),
                topMargin = convertDpToPixel(context, 16F),
                bottomMargin = convertDpToPixel(context, 16F)
            )

            adapter = SubCategoriesAdapter(callback)
            setData(subCategoriesViewModel.subCategories)
        }
    }

    override fun onResume() {
        super.onResume()
        callback.setTitle(category.name)
        callback.setHomeAsUpEnabled(true)
    }


    interface Callback : MainActivityBaseCallback, SubCategoriesAdapter.Callback


    companion object {

        private const val ARG_CATEGORY = "ARG_CATEGORY"

        fun newInstance(category: Category): SubCategoriesFragment = SubCategoriesFragment().apply {
            arguments = bundleOf(ARG_CATEGORY to category)
        }
    }
}