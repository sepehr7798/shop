package ir.bcoders.shop.fragments

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ir.bcoders.shop.MainApplication
import ir.bcoders.shop.R
import ir.bcoders.shop.activities.MainActivityBaseCallback
import ir.bcoders.shop.adapters.HomeItemsAdapter
import ir.bcoders.shop.di.components.DaggerHomeComponent
import ir.bcoders.shop.models.Category
import ir.bcoders.shop.models.HomeItem
import ir.bcoders.shop.models.Product
import ir.bcoders.shop.utils.addSpacingItemDecoration
import ir.bcoders.shop.utils.convertDpToPixel
import ir.bcoders.shop.viewmodels.HomeViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import sb.liverecyclerview.StatableLiveList
import javax.inject.Inject

class HomeFragment : Fragment(), HomeItemsAdapter.DataHelper {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var homeViewModel: HomeViewModel

    private lateinit var callback: Callback

    override fun onAttach(context: Context) {
        super.onAttach(context)
        DaggerHomeComponent.builder()
            .applicationComponent(MainApplication.getApplicationComponent())
            .build()
            .inject(this)

        homeViewModel = ViewModelProvider(this, viewModelFactory).get(HomeViewModel::class.java)

        callback = activity as Callback
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_home, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        callback.setIsAppbarSelected(false)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            home_nested_scroll_view.setOnScrollChangeListener { v, _, _, _, _ ->
                callback.setIsAppbarSelected(v.canScrollVertically(-1))
            }
        }

        home_items_lree_view.run {
            addSpacingItemDecoration(
                convertDpToPixel(context, 16F),
                topMargin = convertDpToPixel(context, 16F),
                bottomMargin = convertDpToPixel(context, 16F)
            )

            adapter = HomeItemsAdapter(callback, this@HomeFragment)
            setData(homeViewModel.homeItems)
        }
    }

    override fun onResume() {
        super.onResume()
        callback.setTitle(getString(R.string.app_name))
        callback.setHomeAsUpEnabled(false)
    }

    override fun getCategories(homeItemCategories: HomeItem.Categories): StatableLiveList<Category> =
        homeViewModel.getCategories(homeItemCategories)

    override fun getProducts(homeItemProducts: HomeItem.Products): StatableLiveList<Product> =
        homeViewModel.getProducts(homeItemProducts)


    interface Callback : MainActivityBaseCallback, HomeItemsAdapter.Callback


    companion object {

        fun newInstance(): HomeFragment = HomeFragment()
    }
}