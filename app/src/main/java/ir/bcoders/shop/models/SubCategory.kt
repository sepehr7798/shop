package ir.bcoders.shop.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubCategory(
    val id: Int,
    val name: String,
    val description: String,
    @SerializedName("image_url") val imageUrl: String,
    @SerializedName("image_tint") val imageTint: Int
) : Parcelable