package ir.bcoders.shop.models

import com.google.gson.annotations.SerializedName

data class Review(
    val id: Int,
    @SerializedName("user_icon_url") val userIconUrl: String,
    @SerializedName("user_name") val userName: String,
    val rate: Float,
    @SerializedName("helpful_count") val helpfulCount: Int,
    val date: Long,
    val text: String
)