package ir.bcoders.shop.models

import com.google.gson.annotations.SerializedName

sealed class HomeItem(open val type: Int) {

    data class Categories(
        val id: Int
    ) : HomeItem(TYPE_CATEGORIES)

    data class Products(
        val id: Int,
        val title: String
    ) : HomeItem(TYPE_PRODUCT_LIST)

    data class Offer(
        val id: Int,
        val title: String,
        val description: String,
        @SerializedName("image_url") val imageUrl: String,
        @SerializedName("image_tint") val imageTint: Int
    ) : HomeItem(TYPE_OFFER)


    companion object {
        const val TYPE_CATEGORIES = 0
        const val TYPE_PRODUCT_LIST = 1
        const val TYPE_OFFER = 2
    }
}