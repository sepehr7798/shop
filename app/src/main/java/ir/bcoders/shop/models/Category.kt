package ir.bcoders.shop.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Category(
    val id: Int,
    val name: String,
    @SerializedName("icon_url") val iconUrl: String
) : Parcelable