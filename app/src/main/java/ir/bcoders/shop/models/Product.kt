package ir.bcoders.shop.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
    val id: Int,
    val name: String,
    @SerializedName("icon_url") val iconUrl: String,
    @SerializedName("avg_rate") val avgRate: Float,
    @SerializedName("review_count") val reviewCount: Int,
    val price: Double,
    val about: String,
    @SerializedName("star_count_1") val starCount1: Int,
    @SerializedName("star_count_2") val starCount2: Int,
    @SerializedName("star_count_3") val starCount3: Int,
    @SerializedName("star_count_4") val starCount4: Int,
    @SerializedName("star_count_5") val starCount5: Int
) : Parcelable